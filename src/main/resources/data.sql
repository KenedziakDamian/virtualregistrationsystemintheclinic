INSERT INTO role (name) VALUES('ADMIN');
INSERT INTO role (name) VALUES('USER');
INSERT INTO role (name) VALUES('DOCTOR');


INSERT INTO doctor_category (name) VALUES('Chirurg');
INSERT INTO doctor_category (name) VALUES('Chirurg plastyczny');
INSERT INTO doctor_category (name) VALUES('Dermatolog');
INSERT INTO doctor_category (name) VALUES('Gastrolog');
INSERT INTO doctor_category (name) VALUES('Kardiolog');
INSERT INTO doctor_category (name) VALUES('Laryngolog');


INSERT INTO users (category_id,is_doctor, name,surname,username,password) VALUES(1,true,'d','d','admin','$2a$04$PDQG8g80wYHeFCf18Fclv.QCy/gbHGuFSFm9OxXRMpE/CYkjNTHZq');
INSERT INTO users_roles(user_id,roles_id) VALUES(1,1);
INSERT INTO users_roles(user_id,roles_id) VALUES(1,3);
