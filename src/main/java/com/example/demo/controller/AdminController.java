package com.example.demo.controller;

import com.example.demo.DTO.NewDoctorDTO;
import com.example.demo.DTO.TermDTO;
import com.example.demo.model.Term;
import com.example.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.LinkedList;
import java.util.List;

@Controller
public class AdminController  {

    @Autowired
    UserService userService;

    @RequestMapping(value = "/admin/addNewDoctor",method = RequestMethod.GET)
    public String addDoctorForm(Model model,RedirectAttributes redirectAttributes) {

        model.addAttribute("DoctorCategories",userService.getAllDoctorCategory());
        model.addAttribute("newDoctorDTO", new NewDoctorDTO());

        return "/admin/addNewDoctor";
    }


    @RequestMapping(value = "/admin/addNewDoctor",method = RequestMethod.POST)
    public String addDoctorFormSubmit(Model model,@Valid NewDoctorDTO newDoctorDTO, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("DoctorCategories",userService.getAllDoctorCategory());
            return "/admin/addNewDoctor";
        }
        userService.addNewDoctor(newDoctorDTO);
        return "/admin/addedNewDoctor";
    }

    @RequestMapping(value = "/admin/reservations",method = RequestMethod.GET)
    public String showReservations(Model model){
        LinkedList<TermDTO> termReservatedDTOList = new LinkedList<>();
        List<Term> reservatedTerms = userService.getAllReservatedTerms();
        reservatedTerms.stream().forEach(e -> termReservatedDTOList.add(e.toDTO()));

        LinkedList<TermDTO> termAcceptedDTOList = new LinkedList<>();
        List<Term> acceptedTerms = userService.getAllAcceptedTerms();
        acceptedTerms.stream().forEach(e -> termAcceptedDTOList.add(e.toDTO()));



        model.addAttribute("reservatedTerms",termReservatedDTOList);
        model.addAttribute("acceptedTerms",termAcceptedDTOList);

        return "/admin/reservations";
    }





    @RequestMapping(value = "/admin/reservation/{termID}/accept/", method = RequestMethod.GET)
    public String acceptReservation(Model model, @PathVariable(value = "termID") Long termID) {
        userService.changeTermStatus(termID, Term.EvailabilityStatus.HIRED);

        return "redirect:/admin/reservations";
    }


    @RequestMapping(value = "/admin/reservation/{termID}/reject/", method = RequestMethod.GET)
    public String rejectReservation(Model model, @PathVariable(value = "termID") Long termID) {
        userService.changeTermStatus(termID, Term.EvailabilityStatus.FREE);

        return "redirect:/admin/reservations";
    }
//
//    @RequestMapping(value = "/admin/addedNewDoctor",method = RequestMethod.POST)
//    public String addedDoctor(Model model,@Valid NewDoctorDTO newDoctorDTO, BindingResult bindingResult) {
//        userService.addNewDoctor(newDoctorDTO);
//        return "/admin/addedNewDoctor";
//    }





}
