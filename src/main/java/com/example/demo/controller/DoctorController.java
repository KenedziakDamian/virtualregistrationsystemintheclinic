package com.example.demo.controller;

import com.example.demo.DTO.NewDoctorDTO;
import com.example.demo.DTO.TermDTO;
import com.example.demo.model.DoctorCategory;
import com.example.demo.model.Term;
import com.example.demo.model.User;
import com.example.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;
import java.util.LinkedList;
import java.util.List;

@Controller
public class DoctorController {
    @Autowired
    UserService userService;


    @RequestMapping(value = "/doctor/addNewTerm",method = RequestMethod.GET)
    public String addNewTermShow(Model model) {
        model.addAttribute("newTermDTO", new TermDTO());
        Object principal  = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal instanceof UserDetails) {
            List<Term> terms = userService.getUserByUsername(((UserDetails) principal).getUsername()).getTerms();

            LinkedList<TermDTO> termDTOList  = new LinkedList<>();

            terms.stream().forEach(e->  termDTOList.add(e.toDTO()) );

            model.addAttribute("listOfTerms",termDTOList);

        }

        return "/doctor/addNewTerm";
    }

    @RequestMapping(value = "/doctor/addNewTerm",method = RequestMethod.POST)
    public String addNewTermFromSubmit(Model model, @Valid TermDTO termDTO, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "/doctor/addNewTerm";
        }
        Object principal  = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal instanceof UserDetails) {
            if(userService.addTerm(((UserDetails) principal).getUsername(),termDTO)) {
                return "/doctor/addedNewTerm";
            }
            model.addAttribute("newTermDTO", new TermDTO());
            return "/doctor/addNewTerm";
        }
        model.addAttribute("newTermDTO", new TermDTO());
        return "/doctor/addNewTerm";

    }

}


