package com.example.demo.controller;


import com.example.demo.DTO.UnregistratedUserDTO;
import com.example.demo.DTO.TermDTO;
import com.example.demo.model.DoctorCategory;
import com.example.demo.model.Term;
import com.example.demo.model.User;
import com.example.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;
import java.util.LinkedList;
import java.util.List;

@Controller
public class UnregistratedUserController {
    @Autowired
    UserService userService;


    @RequestMapping(value = "/doctors/{categoryID}/", method = RequestMethod.GET)
    public String showDoctors(Model model, @PathVariable(value = "categoryID") Long categoryID) {
        DoctorCategory doctorCategory = userService.getCategoryById(categoryID);
        if (doctorCategory == null) {
            return "/";
        }
        model.addAttribute("category", doctorCategory);
        model.addAttribute("doctorList", userService.getDoctorByRoleId(categoryID));
        return "/doctors";
    }

    @RequestMapping(value = "/doctors/profile/{doctorID}/", method = RequestMethod.GET)
    public String showDetailedInformation(Model model, @PathVariable(value = "doctorID") Long doctorID) {
        User user = userService.getDoctorByID(doctorID);
        List<Term> terms = user.getTerms();
        if (user == null) {
            return "/";
        }
        model.addAttribute("doctor", user);

        LinkedList<TermDTO> termDTOList = new LinkedList<>();
        terms.stream().forEach(e -> termDTOList.add(e.toDTO()));

        model.addAttribute("listOfTerms", termDTOList);
        return "/profile";
    }


    @RequestMapping(value = "/reservation/{reservationID}/", method = RequestMethod.GET)
    public String makeReservation(Model model, @PathVariable(value = "reservationID") Long reservationID) {
        Term term = userService.getTermById(reservationID);

        if(term.getId() == null || term.getStatus() != Term.EvailabilityStatus.FREE){
            return "redirect:/homepage";

        }
        model.addAttribute("term", term.toDTO());
        model.addAttribute("newPatientDTO", new UnregistratedUserDTO());
        return "/reservation";
    }



    @RequestMapping(value = "/makeReservation/{reservationID}/",method = RequestMethod.POST)
    public String addNewTermFromSubmit(Model model, @PathVariable(value = "reservationID") Long reservationID, @Valid UnregistratedUserDTO unregistratedUserDTO, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "redirect:/reservation/" + reservationID + "/";
        }
        if (userService.makeReservation(unregistratedUserDTO, reservationID)) {
            model.addAttribute("title", "Sukces!");
            model.addAttribute("content", "Złożyłeś poadnie o przyjęcie na wizytę. Na adres e-mail zostaną wysłane wszystkie potrzebne informacje." +
                    "Gdy wizyta zostanie potwierdzona, zostaniesz o tym poinformowany mailowo.");

            return "/succesInfo";
        }

        return "redirect:/homepage";
    }
}




