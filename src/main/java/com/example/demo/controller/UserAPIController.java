package com.example.demo.controller;

import com.example.demo.model.User;
import com.example.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class UserAPIController {


    UserService userService;

    public UserAPIController(UserService userService) {

        this.userService = userService;
    }

    @RequestMapping(value = "/api/user/addUser/", method = RequestMethod.GET)
    public List<User> addUser() {
        User user = new User();
        user.setName("john");
        user.setPassword("12345");
        user.setPeselNumber(new Long("95102903416"));
        user.setUsername("Johnny");

        userService.update(user);

        return userService.getAllUsers();
    }
}
