package com.example.demo.service;


import com.example.demo.DTO.UnregistratedUserDTO;
import com.example.demo.model.Term;
import com.example.demo.model.UnregisteredUser;
import com.example.demo.repository.TermRepository;
import com.example.demo.repository.UnregistratedUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;

@Service
public class UnregistratedUserService {

    TermRepository termRepository;
    UnregistratedUserRepository unregistratedUserRepository;

    @Autowired
    public UnregistratedUserService(UnregistratedUserRepository unregistratedUserRepository, TermRepository termRepository){
        this.termRepository= termRepository;
        this.unregistratedUserRepository = unregistratedUserRepository;
    }



    @Transactional
    public void reserveTerm(Long termId, UnregistratedUserDTO unregisteredUserDTO){
        Term term  = termRepository.findFirstById(termId);
        if(term == null){
            return;
        }
        UnregisteredUser unregistratedUser  = unregistratedUserRepository.save(new UnregisteredUser(unregisteredUserDTO));
        term.setStatus(Term.EvailabilityStatus.RESERVATED);
        term.setPatient(unregistratedUser);

        //WYSLIJ EMAIL ZE WYSLAL ZAPYTANIE O TERMIN
        termRepository.save(term);
    }


}
