package com.example.demo.service;

import com.example.demo.DTO.NewDoctorDTO;
import com.example.demo.DTO.UnregistratedUserDTO;
import com.example.demo.DTO.TermDTO;
import com.example.demo.StaticRoleNames;
import com.example.demo.model.*;
import com.example.demo.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import javax.validation.Valid;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class UserService{

    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final TermRepository termRepository;
    private final UnregistratedUserRepository unregistratedUserRepository;
    private final DoctorCategoryRepository doctorCategoryRepository;
    @Autowired
    public UserService(DoctorCategoryRepository doctorCategoryRepository, UnregistratedUserRepository unregistratedUserRepository,UserRepository userRepository, RoleRepository roleRepository, TermRepository termRepository) {
        this.userRepository= userRepository;
        this.roleRepository = roleRepository;
        this.termRepository = termRepository;
        this.doctorCategoryRepository = doctorCategoryRepository;
        this.unregistratedUserRepository = unregistratedUserRepository;
    }
    public DoctorCategory getCategoryById(Long id){
        return doctorCategoryRepository.findById(id).orElse(null);
    }

    @Transactional
    public boolean  addTerm(String doctorUsername,TermDTO termDTO){
        User user = userRepository.findFirstByUsername(doctorUsername);

        if(user.getRoles().contains(roleRepository.findByName(StaticRoleNames.DOCTOR_ROLE))){

            java.util.Date date = null;


            try {
                String pattern = "MM/dd/yyyy";
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
                date =  simpleDateFormat.parse(termDTO.getDate());

            } catch (ParseException e) {
                e.printStackTrace();
            }


            java.util.Date finalDate = date;
            if(user.getTerms().stream().anyMatch(t -> t.getDate().equals(finalDate))){
                    return false;
                }

            Calendar cal = Calendar.getInstance();
            String [] splitStart = termDTO.getStartTime().split(":");
            cal.setTime(date);

            cal.set(Calendar.HOUR_OF_DAY,Integer.parseInt(splitStart[0]));
            cal.set(Calendar.MINUTE, Integer.parseInt(splitStart[1]));
            cal.set(Calendar.SECOND, 0);
            cal.set(Calendar.MILLISECOND, 0);
            String [] splitStop= termDTO.getEndTime().split(":");

            java.util.Date startDate = cal.getTime();

            cal.setTime(date);
            cal.set(Calendar.HOUR_OF_DAY,Integer.parseInt(splitStop[0]));
            cal.set(Calendar.MINUTE, Integer.parseInt(splitStop[1]));

            java.util.Date endDate = cal.getTime();
            Integer current = new Integer(1);

            if(Integer.parseInt(splitStart[0])>=Integer.parseInt(splitStop[0])){
                return false;
            }
            while(Integer.parseInt(splitStart[0]) + current != Integer.parseInt(splitStop[0])+1){






                Term term = new Term(date,
                                    Integer.parseInt(splitStart[0])+current-1,
                                    Integer.parseInt(splitStart[0])+current);
                term.setRecipient(user);
                term = termRepository.save(term);
                user.getTerms().add(term);
                userRepository.save(user);
                current+=1;
            }




        }

        return true;


    }

    private User addUser(User user)  {
        Role userRole = roleRepository.findByName(StaticRoleNames.USER_ROLE);
        user.getRoles().add(userRole);
        if(userRepository.findByUsername(user.getUsername()) != null)
            return userRepository.findByUsername(user.getUsername());
        User savedUser = userRepository.save(user);
        return savedUser;
    }
    public User getUserByPeselNumber(Long peselNumber){
        return this.userRepository.findFirstByPeselNumber(peselNumber);
    }
    public User getUserByUsername(String useranme){
        return userRepository.findByUsername(useranme);
    }

    public User getOne(Long id){
        return userRepository.findOneById(id);
    }

    public void addAdmin(User user){
        Role userRole = roleRepository.findByName(StaticRoleNames.ADMIN_ROLE);
        user.getRoles().add(userRole);
        userRepository.save(user);
    }

    public List<Role>getAllRole(){ return roleRepository.findAll();}


    public List<User> getAllUsers() {
        return userRepository.findAll();
    }
    public void addUserAdmin(User user){
        Role userRole = roleRepository.findByName(StaticRoleNames.ADMIN_ROLE);
        Role adminRole= roleRepository.findByName(StaticRoleNames.USER_ROLE);
        user.getRoles().add(userRole);
        user.getRoles().add(adminRole);
        userRepository.save(user);
    }

    public List<DoctorCategory> getAllDoctorCategory(){
        return doctorCategoryRepository.findAll();

    }
    public User update(User user){
       return userRepository.save(user);
    }

    @Transactional
    public void addNewDoctor(@Valid NewDoctorDTO newDoctorDTO) {
        User user = new User(newDoctorDTO);
        user.getRoles().add(roleRepository.findByName(StaticRoleNames.DOCTOR_ROLE));
        user.setCategory(doctorCategoryRepository.findByName(newDoctorDTO.getCategory()));
        userRepository.save(user);
    }

    public List<User> getDoctorByRoleId(Long id){
        DoctorCategory doctorCategory = doctorCategoryRepository.findById(id).orElse(null);
        if (doctorCategory != null) {
            return userRepository.findUsersByCategory(doctorCategory);
        }
        return null;
    }

    public User getDoctorByID(Long doctorID) {
        return userRepository.findOneByIdAndIsDoctorTrue(doctorID);

    }

    public Term getTermById(Long reservationID) {
        return termRepository.getOne(reservationID);
    }

    @Transactional
    public boolean makeReservation(@Valid UnregistratedUserDTO unregistratedUserDTO, Long reservationID) {
        Term term = termRepository.findFirstById(reservationID);
        if(term.getId() == null || term.getStatus() != Term.EvailabilityStatus.FREE){
            return false;
        }
        UnregisteredUser unregisteredUser = new UnregisteredUser(unregistratedUserDTO);
        unregisteredUser = unregistratedUserRepository.save(unregisteredUser);
        term.setPatient(unregisteredUser);
        term.setStatus(Term.EvailabilityStatus.RESERVATED);
        termRepository.save(term);
        return  true;


    }

    public List<Term> getAllReservatedTerms() {
        return termRepository.findAllByStatus(Term.EvailabilityStatus.RESERVATED);
    }


    public List<Term> getAllAcceptedTerms() {
        return termRepository.findAllByStatus(Term.EvailabilityStatus.HIRED);
    }

    @Transactional
    public void changeTermStatus(Long termID, Term.EvailabilityStatus status) {
        Term term = termRepository.findFirstById(termID);
        if(term == null)
            return;

        term.setStatus(status);
        if(status == Term.EvailabilityStatus.FREE){
            //odrzucenie wizyty
            term.setPatient(null);
        }

        if(status == Term.EvailabilityStatus.HIRED){
            //Akceptacja wizyty
        }

        termRepository.save(term);
    }
}
