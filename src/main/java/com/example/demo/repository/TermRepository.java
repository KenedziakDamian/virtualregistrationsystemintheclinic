package com.example.demo.repository;


import com.example.demo.model.Term;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TermRepository extends JpaRepository<Term,Long> {
    Term findFirstById(Long id);
    List<Term> findAllByStatus(Term.EvailabilityStatus Status);
}
