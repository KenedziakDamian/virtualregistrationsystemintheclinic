package com.example.demo.repository;

import com.example.demo.model.UnregisteredUser;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UnregistratedUserRepository extends JpaRepository<UnregisteredUser,Long> {

}
