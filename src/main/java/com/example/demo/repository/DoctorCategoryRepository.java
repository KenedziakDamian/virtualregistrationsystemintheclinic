package com.example.demo.repository;


import com.example.demo.model.DoctorCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DoctorCategoryRepository  extends JpaRepository<DoctorCategory,Long> {
    DoctorCategory findByName (String name);
}

