package com.example.demo.repository;

import com.example.demo.model.DoctorCategory;
import com.example.demo.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Locale;


@Repository
public interface UserRepository extends JpaRepository<User,Long> {
    User findOneById(Long id);
    User findOneByIdAndIsDoctorTrue(Long id);
    User findByUsername(String username);
    User findFirstByUsername(String username);
    User findFirstByPeselNumber(Long pesel);
    List<User>findUsersByCategory(DoctorCategory category);

}
