package com.example.demo.model;

import com.example.demo.DTO.TermDTO;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.sql.Date;
import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name ="terms")
public class Term {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true)
    private Long id;

    private java.util.Date date;

    private Integer startTime;

    private Integer endTime;

    @Enumerated(EnumType.STRING)
    private  EvailabilityStatus status;


    @OneToOne
    @JoinColumn(name="users_id")
    private User recipient;

    @OneToOne
    @JoinColumn(name="unregisteredUsers_id")
    private UnregisteredUser patient = null;


    public static enum EvailabilityStatus {
        FREE,HIRED,RESERVATED

    }

    public Term(java.util.Date date, Integer startTime, Integer endTime){
        this.date = date;
        this.startTime = startTime;
        this.endTime = endTime;
        status = EvailabilityStatus.FREE;


    }

    public TermDTO toDTO(){
        TermDTO termDTO = new TermDTO();
        termDTO.setDate(date.toString().split(" ")[0]);
        termDTO.setId(id);
        termDTO.setDoctorName(this.getRecipient().getName());
        termDTO.setDoctorSurname(this.getRecipient().getSurname());
        termDTO.setStartTime(this.startTime.toString());
        termDTO.setEndTime(this.endTime.toString());
        termDTO.setStatus(this.getStatus().name());

        if(patient !=null) {
            termDTO.setPatientEmail(patient.getEmailAdress());
            termDTO.setPatientPesel(String.valueOf(patient.getPeselNumber()));
            termDTO.setPatientPhoneNumber(String.valueOf(patient.getPhoneNumber()));
            termDTO.setPatientSurname(patient.getName());
            termDTO.setPatienttName(patient.getSurname());
        }
        return  termDTO;

    }
}