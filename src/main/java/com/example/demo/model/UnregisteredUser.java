package com.example.demo.model;

import com.example.demo.DTO.UnregistratedUserDTO;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.LinkedList;
import java.util.List;
@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name ="unregisteredUsers")
public class UnregisteredUser {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true)
    private Long id;



    @NotNull
    private Long peselNumber;
    @NotEmpty
    private String name;
    @NotEmpty
    private String surname;
    @NotEmpty
    @Email
    private String emailAdress;
    @NotNull
    @Column(length = 9)
    private Long phoneNumber;


     public UnregisteredUser (UnregistratedUserDTO unregistratedUserDTO){
        this.name = unregistratedUserDTO.getName();
        this.surname = unregistratedUserDTO.getSurname();
        this.emailAdress = unregistratedUserDTO.getEmail();
        this.phoneNumber = new Long(unregistratedUserDTO.getPhoneNumber());
        this.peselNumber = new Long(unregistratedUserDTO.getPesel());

    }
    public UnregistratedUserDTO toDTO(){
         UnregistratedUserDTO unregistratedUserDTO = new UnregistratedUserDTO();
         unregistratedUserDTO.setEmail(this.emailAdress);
         unregistratedUserDTO.setName(this.name);
         unregistratedUserDTO.setSurname(this.surname);
         unregistratedUserDTO.setPesel(String.valueOf(this.peselNumber));
         unregistratedUserDTO.setPhoneNumber(String.valueOf(this.phoneNumber));
         return unregistratedUserDTO;

    }
}


