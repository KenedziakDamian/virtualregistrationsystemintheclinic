package com.example.demo.model;

import com.example.demo.DTO.NewDoctorDTO;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.*;
import java.util.LinkedList;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name ="users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true)
    private Long id;

    private Long peselNumber;
    private String name;
    private String surname;
    private Long phoneNumber;

    private Boolean isDoctor = false;
    @NotNull
    @NotEmpty
    private  String username;
    private String password;

    @Email
    private String email;
    @ManyToMany(fetch=FetchType.EAGER)
    private List<Role> roles = new LinkedList<>();


    @ManyToMany
    private List<Term> terms = new LinkedList<>();


    @ManyToOne
    private DoctorCategory category;

    public User(@Valid NewDoctorDTO newDoctorDTO) {
        this.name = newDoctorDTO.getName();
        this.surname = newDoctorDTO.getSurname();
        this.username = new String(newDoctorDTO.getSurname()+newDoctorDTO.getName()).toLowerCase();
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        this.password = bCryptPasswordEncoder.encode(newDoctorDTO.getPassword());
        this.email = newDoctorDTO.getEmail();
        this.isDoctor = true;

    }


}
