package com.example.demo.DTO;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
@Getter
@Setter
public class UnregistratedUserDTO {


    @NotEmpty(message = "")
    String pesel;

    @NotEmpty(message = "{newDoctorDTO.name.notempty}")
    String name;

    @NotEmpty(message = "{newDoctorDTO.surname.notempty}")
    String surname;

    @Email
    @NotEmpty(message = "{newDoctorDTO.email.notempty}")
    String email;

    @NotEmpty(message ="")
    String phoneNumber;
}


