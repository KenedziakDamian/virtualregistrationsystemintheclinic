package com.example.demo.DTO;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import java.sql.Date;


@Getter
@Setter
public class TermDTO {

    Long id;
    @NotEmpty(message = "{termDTO.date.notempty}")
    String date;
    @NotEmpty(message = "{termDTO.startTime.notempty}")
    String startTime;
    @NotEmpty(message = "{termDTO.endTime.notempty}")
    String endTime;
    String status;


    String doctorName;
    String doctorSurname;


    String patienttName;
    String patientSurname;
    String patientPesel;
    String patientEmail;
    String patientPhoneNumber;
}


